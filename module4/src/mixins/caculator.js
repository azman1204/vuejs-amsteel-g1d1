export default {
    data() {
        return {
            name: 'John Doe'
        }
    },
    methods: {
        sum(num1, num2) {
            return num1 + num2;
        },
        calc(num1, num2) {  
            console.log(this.sum(num1, num2)); 
        }
    }
}