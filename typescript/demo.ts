// TypeScript types
const myname:string = "Azman";
const num:number = 10;

//myname = 10; // error

// Array types
const primeNumber: number[] = [1, 3, 5, 7];
console.log(primeNumber[1]);

// Tuples types
let person: [string, number];
person = ["Azman", 30];
console.log(`My name is ${person[0]} and i am ${person[1]} years old`);

// Enum
enum ErrorLevel {
    Info = 10, // 0
    Debug, // 1
    Warning, // 2
    Error, // 3
    Critical // 4
}

console.log("Error Level: " + ErrorLevel.Debug);

// Any
let mayBeis: any = 4;
mayBeis = 'Any string';
mayBeis = true;

// Object types
interface IPerson {
    name: string;
    age: number;
}

// literal object
const people: IPerson = {
    name: 'Azman',
    age: 30
}

// function
function sum(a: number, b: number = 0): number {
    return a + b;
}

sum(5);
sum(5, 10);
//sum(); error