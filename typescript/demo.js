// TypeScript types
var myname = "Azman";
var num = 10;
//myname = 10; // error
// Array types
var primeNumber = [1, 3, 5, 7];
console.log(primeNumber[1]);
// Tuples types
var person;
person = ["Azman", 30];
console.log("My name is ".concat(person[0], " and i am ").concat(person[1], " years old"));
// Enum
var ErrorLevel;
(function (ErrorLevel) {
    ErrorLevel[ErrorLevel["Info"] = 10] = "Info";
    ErrorLevel[ErrorLevel["Debug"] = 11] = "Debug";
    ErrorLevel[ErrorLevel["Warning"] = 12] = "Warning";
    ErrorLevel[ErrorLevel["Error"] = 13] = "Error";
    ErrorLevel[ErrorLevel["Critical"] = 14] = "Critical"; // 4
})(ErrorLevel || (ErrorLevel = {}));
console.log("Error Level: " + ErrorLevel.Debug);
