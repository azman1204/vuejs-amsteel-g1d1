import { Animal, FoodChainType } from "./Animal";

class Dog extends Animal {
  constructor() {
    super({
      name: "Dog",
      sound: "Wof!",
      family: "Canidae",
      foodChainType: FoodChainType.Carnivorous,
    });
  }
}

let dog: Dog = new Dog();
dog.whoAmI();
